import logging
logging.basicConfig(level=logging.DEBUG)
from spyne import Application, rpc, ServiceBase, \
    Integer, Unicode
from spyne import Iterable
from spyne.protocol.http import HttpRpc
from spyne.protocol.json import JsonDocument
from spyne.server.wsgi import WsgiApplication
import urllib2
import json
from collections import Counter
from datetime import datetime, time

class HelloWorldService(ServiceBase):
    @rpc(Unicode, Unicode, Unicode, _returns=Iterable(Unicode))
    def checkcrime(ctx, lat, lon, radius):
        url = "https://api.spotcrime.com/crimes.json?"
        final_url = url + "lat=" + str(lat) + "&lon=" + str(lon) + "&radius=" + str(radius) + "&key=."
        json_obj = urllib2.urlopen(final_url)
        data = json.load(json_obj)
        check_time1 = datetime.strptime('00:00:00', '%H:%M:%S').time()
        check_time2 = datetime.strptime('3:00:00', '%H:%M:%S').time()
        check_time3 = datetime.strptime('3:01:00', '%H:%M:%S').time()
        check_time4 = datetime.strptime('6:00:00', '%H:%M:%S').time()
        check_time5 = datetime.strptime('6:01:00', '%H:%M:%S').time()
        check_time6 = datetime.strptime('9:00:00', '%H:%M:%S').time()
        check_time7 = datetime.strptime('9:01:00', '%H:%M:%S').time()
        check_time8 = datetime.strptime('12:00:00', '%H:%M:%S').time()
        check_time9 = datetime.strptime('12:01:00', '%H:%M:%S').time()
        check_time10 = datetime.strptime('15:00:00', '%H:%M:%S').time()
        check_time11 = datetime.strptime('15:01:00', '%H:%M:%S').time()
        check_time12 = datetime.strptime('18:00:00', '%H:%M:%S').time()
        check_time13 = datetime.strptime('18:01:00', '%H:%M:%S').time()
        check_time14 = datetime.strptime('21:00:00', '%H:%M:%S').time()
        check_time15 = datetime.strptime('21:01:00', '%H:%M:%S').time()
        check_time16 = datetime.strptime('23:59:00', '%H:%M:%S').time()
        count1 = 0
        count2 = 0
        count3 = 0
        count4 = 0
        count5 = 0
        count6 = 0
        count7 = 0
        count8 = 0
        data_crime = data['crimes']
        counter_crimes = Counter(d['type'] for  d in data_crime)
        total_crime = sum(counter_crimes.itervalues())
        most_crimes = Counter(d['address'] for  d in data_crime)
        crime_type_count = dict(counter_crimes)
        top_crimes = (most_crimes.most_common(3))
        top_crimes = [elem[0] for elem in top_crimes]
        dates= [d['date'] for d in data_crime]
        dates_list = [datetime.strptime(date, "%m/%d/%y %I:%M %p").time() for date in dates]
        for i in dates_list:
            if check_time1 <= i <= check_time2:
                count1 +=1
            elif check_time3 <= i <= check_time4:
                count2 +=1
            elif check_time5 <= i <= check_time6:
                count3 +=1
            elif check_time7 <= i <= check_time8:
                count4 +=1
            elif check_time9 <= i <= check_time10:
                count5 +=1
            elif check_time11 <= i <= check_time12:
                count6 +=1
            elif check_time13 <= i <= check_time14:
                count7 +=1
            elif check_time15 <= i <= check_time16:
                count8 +=1
        output = {
            "Total_crime" : total_crime,
            "the_most_dangerous_streets" : top_crimes,
            "crime_type_count" : crime_type_count,
            "event_time_count" : {
                "12:01am-3am" : count1,
                "3:01am-6am" : count2,
                "6:01am-9am" : count3,
                "9:01am-12noon" : count4,
                "12:01pm-3pm" : count5,
                "3:01pm-6pm" : count6,
                "6:01pm-9pm" : count7,
                "9:01pm-12midnight" : count8
            }
        }
        yield output 
application = Application([HelloWorldService],
    tns='spyne.examples.hello',
    in_protocol=HttpRpc(validator='soft'),
    out_protocol=JsonDocument()
)
if __name__ == '__main__':
    # You can use any Wsgi server. Here, we chose
    # Python's built-in wsgi server but you're not
    # supposed to use it in production.
    from wsgiref.simple_server import make_server
    wsgi_app = WsgiApplication(application)
    server = make_server('0.0.0.0', 8000, wsgi_app)
    server.serve_forever()
